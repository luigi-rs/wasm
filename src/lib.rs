pub use common::*;

extern "C" {
	fn raw_log(s: *const u8, l: usize);
}

pub fn console_log(s: &str) {
	unsafe {
		raw_log(s.as_ptr(), s.len());
	}
}
